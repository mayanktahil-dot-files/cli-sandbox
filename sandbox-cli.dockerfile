# Build Container
#
# export DEFAULT_CLI_NAME = "local-cli"
#
# docker build \
#   -t $DEFAULT_CLI_IMAGE \
#   -f sandbox-cli.dockerfile \
#   .
#
# Run Container: 
#
# docker run -it \
#   --rm \
#   --privileged \
#   --name="local_cli" \
#   --hostname=cli \
#   --user=admin \
#   -v /var/run/docker.sock:/var/run/docker.sock \
#   -v $HOME/.ssh:/home/admin/.ssh:ro \
#   -v "$(which docker):/usr/bin/docker" \
#   -v "$PROJECTS:/projects" \
#   -v "$(pwd):/dir" \
#   -v "$HOME/.gitconfig:/home/admin/.gitconfig:ro" \
#   -v $KUBECONFIG:/kube/config \
#   -v $GCLOUD_CONFIG:/home/admin/.config \
#   -l shell_managed \
#   -l shell_default \
#   $DEFAULT_CLI_IMAGE zsh

# Enter into the CLI	
#
# docker exec -it --user=admin sandbox /bin/zsh

FROM docker:stable as static-docker-source

FROM alpine:3.11

ENV OMZ_BRANCH work-macbook
ENV OMZ_GIT_URL https://gitlab.com/mayanktahil-dot-files/oh-my-zsh.git
ENV ZSHRC_GIT_URL https://gitlab.com/mayanktahil-dot-files/zshrc.git
ENV PROJECTS /projects

# Install Gcloud SDK
ARG CLOUD_SDK_VERSION=284.0.0
ENV CLOUD_SDK_VERSION=$CLOUD_SDK_VERSION
ENV CLOUDSDK_PYTHON=python3

ENV PATH /google-cloud-sdk/bin:$PATH
COPY --from=static-docker-source /usr/local/bin/docker /usr/local/bin/docker
RUN apk upgrade --update-cache --available && \
    apk add \
        sudo \
        curl \
        python3 \
        py3-crcmod \
        bash \
        libc6-compat \
        openssh-client \
        git \
        gnupg \
        screen \
 		htop \
 		nano \
 		coreutils \
 		bash \
 		zsh \
 		make \
 		autossh \
        openssl \
 		shadow \
        zsh-vcs \
        fzf && \
        rm -rf /var/cache/apk/*

RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    tar xzf google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    rm google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image && \
    gcloud --version

# Add user "admin" (sudo)
RUN adduser -s /bin/zsh -D admin && \
 adduser admin root && \
 echo "admin            ALL = (ALL) NOPASSWD: ALL" >> /etc/sudoers

# Set up Nano text edtor with Syntax Highlighting 
RUN git clone https://github.com/scopatz/nanorc.git /usr/share/nano
WORKDIR /usr/share/nano
RUN make install && ./install

# Install kubctl in the container and set default config file in /kube directory
WORKDIR /usr/local/bin
RUN  curl -O https://storage.googleapis.com/kubernetes-release/release/v1.7.1/bin/linux/amd64/kubectl && \
 chmod +x kubectl && mkdir /kube && \
 echo 'export KUBECONFIG=/kube/config' >> /etc/profile

RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | sh

USER admin
# Install Oh My Zsh and remove ~/.oh-my-zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" && \
	rm -rf /home/admin/.oh-my-zsh && \
	git clone $OMZ_GIT_URL /home/admin/.oh-my-zsh

# Pull down custom configs from git
WORKDIR /home/admin/.oh-my-zsh	
RUN git pull && git branch && git remote -v && \
	git checkout ${OMZ_BRANCH} && \
	git reset --hard origin/${OMZ_BRANCH}

# Update .zshrc and helper files into home directory. 
WORKDIR /home/admin
RUN git clone $ZSHRC_GIT_URL --depth 1 /tmp/temp && \
    mv /tmp/temp/* /home/admin/ && \
    rm -rf /tmp/temp && \
    touch /home/admin/.nanorc && \
    echo 'include "/usr/share/nano/*.nanorc"' >> /home/admin/.nanorc

# Parents folder mount for your local git projects
VOLUME /projects

# Current working directory volume mount in the container
VOLUME /dir

WORKDIR /projects